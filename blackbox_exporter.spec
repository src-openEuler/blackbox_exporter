%define debug_package %{nil}

Name:                blackbox_exporter
Version:             0.25.0
Release:             1
Summary:             Prometheus blackbox prober exporter
License:             Apache-2.0
URL:                 https://github.com/prometheus/blackbox_exporter
Source0:             https://github.com/prometheus/blackbox_exporter/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:             %{name}.service
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf blackbox_exporter-vendor.tar.gz vendor
Source2:             blackbox_exporter-vendor.tar.gz
Patch0:              Delete-strip-and-pie.patch
BuildRequires:       golang promu
Requires(pre):       prometheus2
%description
The blackbox exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP and ICMP.

%prep
%setup -q -n %{name}-%{version}
tar xvf %{SOURCE2} -C .
%patch 0 -p1

%build
export GOFLAGS="-mod=vendor"
promu build

%install
install -D -m 0755 %{name}-%{version} %{buildroot}%{_bindir}/%{name}
install -D -m 0644 blackbox.yml %{buildroot}%{_sysconfdir}/prometheus/blackbox.yml
install -D -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{name}.service

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%files
%doc LICENSE README.md CONFIGURATION.md example.yml
%{_bindir}/*
%{_unitdir}/%{name}.service
%config(noreplace) %{_sysconfdir}/prometheus/blackbox.yml

%changelog
* Thu Jul 11 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.25.0-1
- Update package to version 0.25.0
- Allow to get Probe logs by target
- Prevent logging confusing error message

* Tue Jun 25 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.24.0-2
- Allow to get Probe logs by target

* Tue Jan 02 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 0.24.0-1
- Update package to version 0.24.0

* Fri Mar 10 2023 wulei <wulei80@h-partners.com> - 0.23.0-3
- Delete linkmode

* Thu Mar 02 2023 wulei <wulei80@h-partners.com> - 0.23.0-2
- Add strip and pie

* Tue Jan 31 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 0.23.0-1
- Update package to version 0.23.0

* Tue Aug 10 2021 huanghaitao <huanghaitao8@huawei.com> - 0.19.0-2
- Fix systemd macro spelling errors.

* Thu Aug 5 2021 huanghaitao <huanghaitao8@huawei.com> - 0.19.0-1
- package init
